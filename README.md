# 汉盲转换 Python 模块

基于 www.braille.org.cn 的 http 接口实现的 汉盲转换 Python 模块



## 依赖

- requests
- beautifulsoup4
- lxml



## 示例代码

文本转盲文见 `text2braille-example.py`

文件转盲文见 `file2braille-example.py`



## 网站和端口

- URL: http://www.braille.org.cn
- 端口: 8080



## 网页接口

### 录入文字

tips：这两个接口的主要区别是 POST 时 数据里的 **flag** 的值为1还是2, 实际上是同一个接口。

- 转换为盲文

    - URL: `/braille-web/braille/textToBraille.html`
    
    - 方法: `POST`

    - 数据: **urlencoded**

        - 示例

          ```markdown
          "userId" = "0"
          "text" = "你好"
          "firstLine" = "on"
          "blankBefore" = "off"
          "blankAfter" = "off"
          "backLine" = "off"
          "shows" = "on"
          "position" = "1"
          "format" = "1"
          "aligned" = "1"
          "pageLine" = "32"
          "show" = "1"
          "cnType" = "1"
          "enType" = "1" 
          "rectNum" = "32"
          "breakLine" = "1"
          "flag" = "2"
          ```
        
    - 返回: **json**
    
      - 示例
    
        ```json
        {
            "status":"200",
            "info":{
                "pureBraillePath":"http://www.braille.org.cn:8080/braille-web/downBraille/asciiAndBraille?path=/opt/uploads/202201202125036827.brf",
                "xmlBraillePath":"http://www.braille.org.cn:8080/braille-web/downBraille/asciiAndBraille?path=/opt/uploads/202201202125036827.xml",
                "braille":"<p>1<\/p><table style= \"margin-left:50px;\"><tr><td style=\"text-align:left;font-size:32px\"> <\/td><td style=\"text-align:left;font-size:32px\"> <\/td><td style=\"text-align:left;font-size:32px\">⠝⠊⠄⠓⠖⠄<\/td><\/tr><\/table>"
            }
        }
        ```
    
        


- 转换为ASCII盲文

  - URL: `/braille-web/braille/textToBraille.html`

  - 方法: `POST`

  - 数据: **urlencoded**

    - 示例

      ```
      "userId" = "0"
      "text" = "你好"
      "firstLine" = "on"
      "blankBefore" = "off"
      "blankAfter" = "off"
      "backLine" = "off"
      "shows" = "on"
      "position" = "1"
      "format" = "1"
      "aligned" = "1"
      "pageLine" = "32"
      "show" = "1"
      "cnType" = "1"
      "enType" = "1"
      "rectNum" = "32"
      "breakLine" = "1"
      "flag" = "1"
      ```

  - 返回: **json**

    - 示例

      ```json
      {
          "status":"200",
          "info":{
              "pureAsciiPath":"http://www.braille.org.cn:8080/braille-web/downBraille/asciiAndBraille?path=/opt/uploads/202201202125059160.brf",
              "xmlAsciiPath":"http://www.braille.org.cn:8080/braille-web/downBraille/asciiAndBraille?path=/opt/uploads/202201202125059160.xml",
              "braille":"<p>1<\/p><table style= \"margin-left:50px;\"><tr><td style=\"text-align:left;font-size:32px\"> <\/td><td style=\"text-align:left;font-size:32px\"> <\/td><td style=\"text-align:left;font-size:32px\">ni'h6'<\/td><\/tr><\/table>"
          }
      }
      ```


### 上传文件

- 上传文件

  - URL: `/braille-web/braille/upload.html`

  - 方法: `POST`

  - 数据: **multipart/form-data**

    - 示例

      ```
      ------WebKitFormBoundary29NitcMsvdoOukQZ
      Content-Disposition: form-data; name="userId"
      
      0
      ------WebKitFormBoundary29NitcMsvdoOukQZ
      Content-Disposition: form-data; name="file"; filename="乌鸦喝水.txt"
      Content-Type: text/plain
      
      《乌鸦喝水》
      一只乌鸦口渴了，到处找水喝。
      乌鸦看见一个瓶子。瓶子里有水，可是瓶子很高，瓶口很小，里边的水又少，它喝不着水。怎么办呢？
      乌鸦看见旁边有许多小石子，它想了一想，有办法了！
      乌鸦把小石子一个一个地衔起来，放到瓶子里。瓶子里的水慢慢升高，乌鸦就喝着水了。 
      
      ------WebKitFormBoundary29NitcMsvdoOukQZ--
      ```

  - 返回: **json**

    - 示例

      ```json
      {
          "status":"200",
          "info":{
              "overlarge":"文件大小合适",
              "filePath":"/opt/uploads/202201220941319905.txt",
              "FileId":142691
          }
      }
      ```


- 文件转盲文

  - URL: `/braille-web/braille/fileToBraille.html`

  - 方法: `GET`

  - 参数: 

    - 示例

      ```
      id=142691
      path=%2Fopt%2Fuploads%2F202201220941319905.txt
      firstLine=on
      blankBefore=off
      blankAfter=off
      backLine=off
      shows=on
      position=1
      format=1
      aligned=1
      pageLine=32
      show=1
      cnType=1
      enType=1
      rectNum=32
      breakLine=1
      flag=2
      ```

  - 返回: **json**

    - 示例

      ```json
      {
          "status":"200",
          "info":{
              "pureBraillePath":"http://www.braille.org.cn:8080/braille-web/downBraille/asciiAndBraille?path=/opt/uploads/202201202125036827.brf",
              "xmlBraillePath":"http://www.braille.org.cn:8080/braille-web/downBraille/asciiAndBraille?path=/opt/uploads/202201202125036827.xml",
              "braille":"<p>1<\/p><table style= \"margin-left:50px;\"><tr><td style=\"text-align:left;font-size:32px\"> <\/td><td style=\"text-align:left;font-size:32px\"> <\/td><td style=\"text-align:left;font-size:32px\">⠝⠊⠄⠓⠖⠄<\/td><\/tr><\/table>"
          }
      }
      ```

- 文件转ASCII盲文

  - URL: `/braille-web/braille/fileToBraille.html`

  - 方法: `GET`

  - 参数: 

    - 示例

      ```
      id=142691
      path=%2Fopt%2Fuploads%2F202201220941319905.txt
      firstLine=on
      blankBefore=off
      blankAfter=off
      backLine=off
      shows=on
      position=1
      format=1
      aligned=1
      pageLine=32
      show=1
      cnType=1
      enType=1
      rectNum=32
      breakLine=1
      flag=1
      ```

  - 返回: **json**

    - 示例

      ```json
      {
          "status":"200",
          "info":{
              "pureAsciiPath":"http://www.braille.org.cn:8080/braille-web/downBraille/asciiAndBraille?path=/opt/uploads/202201202125059160.brf",
              "xmlAsciiPath":"http://www.braille.org.cn:8080/braille-web/downBraille/asciiAndBraille?path=/opt/uploads/202201202125059160.xml",
              "braille":"<p>1<\/p><table style= \"margin-left:50px;\"><tr><td style=\"text-align:left;font-size:32px\"> <\/td><td style=\"text-align:left;font-size:32px\"> <\/td><td style=\"text-align:left;font-size:32px\">ni'h6'<\/td><\/tr><\/table>"
          }
      }
      ```

## 

## 盲文格式

| 格式                                        | 值                    |                      |                |                       |                        |
| ------------------------------------------- | --------------------- | -------------------- | -------------- | --------------------- | ---------------------- |
| cnType                                      | 1; 现行盲文（全带调） | 2; 国家通用盲文      | 3; 双拼盲文    | 4; 表意盲文           |                        |
| enType                                      | 1; 英语一级           | 2; 英语二级          |                |                       |                        |
| rectNum                                     | n; 显示方数           |                      |                |                       |                        |
| flag                                        | 1; 换为ASCII          | 2; 预览盲文          |                |                       |                        |
| 段落设置                                    |                       |                      |                |                       |                        |
| firstLine(首行缩进-首行缩进两个字符)        | on/off                |                      |                |                       |                        |
| blankBefore(段落空行-在每段前增加一个空行)  | on/off                |                      |                |                       |                        |
| blankAfter(段后空行-在每段后增加一个空行)   | on/off                |                      |                |                       |                        |
| backLine(回行缩进-每次回行后缩进规定的方数) | on/off                |                      |                |                       |                        |
| 页码设置                                    |                       |                      |                |                       |                        |
| shows                                       | on; 显示页码          |                      |                |                       |                        |
| position(位置)                              | 1; 页面顶端(页眉)     | 2; 页面低端(页脚)(?) |                |                       |                        |
| format(页码格式)                            | 1; 阿拉伯数字         | 2; 罗马数字(?)       | 3; 盲文数字(?) |                       |                        |
| aligned(对齐方式)                           | 1; 右对齐             | 2; 左对齐(?)         | 3; 中对齐(?)   | 4; 奇数页右，偶数页左 | 5;  奇数页左，偶数页右 |
| pageLine                                    | n; 行数显示           |                      |                |                       |                        |
| 换行方式                                    |                       |                      |                |                       |                        |
| breakLine                                   | 1; 按词换行           | 2; 按字换行(?)       |                |                       |                        |























