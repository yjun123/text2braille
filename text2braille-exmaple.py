from text2braille import Text2Braille

def main():
    t = Text2Braille()

    # ASCII 盲文
    b = t.text2AsciiBraille('''《乌鸦喝水》
一只乌鸦口渴了，到处找水喝。
乌鸦看见一个瓶子。瓶子里有水，可是瓶子很高，瓶口很小，里边的水又少，它喝不着水。怎么办呢？
乌鸦看见旁边有许多小石子，它想了一想，有办法了！
乌鸦把小石子一个一个地衔起来，放到瓶子里。瓶子里的水慢慢升高，乌鸦就喝着水了。 ''')

    # 未处理的xml格式盲文
    # <td style="text-align:left;font-size:32px">hv2m81</td>...
    print(b.braille)
    # 以列表形式返回盲文
    # ['hv2m81', 'fvai2', "hi2t4'", 'n#1g(2', ':1h%2', '/4a31', 'h51', '*a31', 'd62', 'm8131', 'd5', 'z2d42', '/]\'h]2"']
    print(b.braille_list)
    # 以字符串形式返回盲文
    # "- ua$a h5a :w' -"
    # ia /' ua$a k('k5' l5" d62qu2 /6':w' h5a"2
    # ua$a kv2g%2 ia g52 p*1z"2p*1z li' \':w'" k5':2 p*1z h0' g6a" p*1k(' h0' h>'" li'b%a d5 :w' \2 :6'" t9a h5a bu2 /o1 :w'"2z0'm5 bv2 n5"'
    # ua$a kv2g%2 p81b%a \' h+'doa h>':1z'" t9a hx' l5 ia hx'" \' bv2f9' l5;1
    # ua$a b9' h>':1z' i1g52i1g52di2 h%1 ki'l[1" f82d62 p*1z li'"2p*1z li' d5 :w' mv2mv2 :#ag6a" ua$a g\2 h5a /o1 :w' l5"2
    print(b.braille_str)

if __name__ == '__main__':
    main()