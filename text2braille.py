import requests
from bs4 import BeautifulSoup
from copy import deepcopy
from log import logger

DEFAULT_BRAILLE_FORMAT = {
    "userId" : "0",
    "text" : '',
    "firstLine" : "on",
    "blankBefore" : "off",
    "blankAfter" : "off",
    "backLine" : "off",
    "show" : "1",
    "cnType" : "1",
    "enType" : "1",
    "rectNum" : "32",
    "breakLine" : "1",
    "flag" : "1",
}

class FileUploadResp(dict):
    @property
    def status(self):
        """ 状态返回值，正常为 200 """
        return self['status']
    
    @property
    def overlarge(self):
        """ 文件是否符合 """
        return self['info']['overlarge']

    @property
    def filePath(self):
        """ 文件上传后的URL """
        return self['info']['filePath']

    @property
    def FileId(self):
        """ 文件ID """
        return self['info']['FileId']

class Braille(dict):

    @property
    def status(self):
        """ 状态返回值，正常为 200 """
        return self['status']

    @property
    def pureBraillePath(self):
        """ 返回brf(Braille Formatted File)格式盲文的下载地址 """
        return self['info']['pureBraillePath']

    @property
    def xmlBraillePath(self):
        """ 返回xml格式盲文的下载地址 """
        return self['info']['xmlBraillePath']

    @property
    def pureAsciiPath(self):
        """ 返回brf(Braille Formatted File)格式ASCII盲文的下载地址 """
        return self['info']['pureAsciiPath']

    @property
    def xmlAsciiPath(self):
        """ 返回xml格式ASCII盲文的下载地址 """
        return self['info']['xmlAsciiPath']

    @property
    def braille(self):
        """ 返回xml格式盲文 """
        return self['info']['braille']
    
    @property
    def braille_list(self):
        """ 以列表形式返回盲文 """
        soup = BeautifulSoup(self.braille, 'lxml')
        b = [c.get_text() for c in soup.find_all('td')]

        return b

    @property
    def braille_str(self):
        """ 以字符串形式返回盲文 """
        
        return ' '.join(self.braille_list).replace('     ', '\n')[4:]

class Text2Braille():
    def __init__(
        self,
        host: str = 'www.braille.org.cn',
        port: int = 8080,
        timeout: int = 10
    ):
        """ 初始化
        host: 网站域名， 默认为 www.braille.org.cn
        port: 端口, 默认为 8080
        timeout: 超时时间, 默认为 10s
        """
        self.host = host
        self.port = port
        self.timeout = timeout
        self.client = requests.Session()

    def text2Braille(
        self,
        text: str
    ):
        """ 文字转盲文
        text: 需要转换的文字
        """
        data = deepcopy(DEFAULT_BRAILLE_FORMAT)
        data.update({
            'text': text,
            'flag': 2
        })

        return Braille(
            self.textToBraille(data)
        )

    def text2AsciiBraille(
        self,
        text: str
    ):
        """ 文字转ASCII盲文
        text: 需要转换的文字
        """
        data = deepcopy(DEFAULT_BRAILLE_FORMAT)
        data.update({
            'text': text,
            'flag': 1
        })

        return Braille(
            self.textToBraille(data)
        )
    
    def uploadFile(
        self,
        filename
    ):
        """ 上传文件 
        filename: 文件名/路径
        """
        files = {
            'userId': (None, '0'),
            'file': (filename, open(filename, 'rb'), 'text/plain')
        }
        
        return FileUploadResp(
            self.upload(
                files= files
            )
        )

    def file2Braille(
        self,
        id: int,
        path: str
    ):
        """ 文件转盲文
        id： 文件ID, 由 uploadFile 返回的 FileId
        path: 上传文件路径, 由 uploadFile 返回的 filePath
        """
        params = deepcopy(DEFAULT_BRAILLE_FORMAT)
        params.update({
            'id': id,
            'path': path,
            'flag': 2
        })

        return Braille(
            self.fileToBraille(
                params= params
            )
        )

    def file2AsciiBraille(
        self,
        id: int,
        path: str
    ):
        """ 文件转ASCII盲文
        id： 文件ID, 由 uploadFile 返回的 FileId
        path: 上传文件路径, 由 uploadFile 返回的 filePath
        """
        params = deepcopy(DEFAULT_BRAILLE_FORMAT)
        params.update({
            'id': id,
            'path': path,
            'flag': 1
        })

        return Braille(
            self.fileToBraille(
                params= params
            )
        )

    def textToBraille(
        self,
        data: dict
    ):
        """ 文字转盲文基本接口函数 
        data：文字，格式数据
        """
        return  self.post(
            '/braille-web/braille/textToBraille.html',
            payload= data,
        )

    def upload(
        self,
        files
    ):
        """ 文件上传接口函数
        files: 文件，格式数据
        """
        return self.post(
                '/braille-web/braille/upload.html',
                files= files
        )

    def fileToBraille(
        self,
        params: dict
    ):
        """ 文件转盲文基本接口函数 
        params：文件名，格式数据
        """
        return self.get(
            '/braille-web/braille/fileToBraille.html',
            params = params,
        )
    #######################
    def baseRequest(
        self,
        method: str,
        path: str,
        params: dict = None,
        payload: dict = None,
        headers: dict = None,
        files: dict = None,
    ) -> dict:
        """基础请求方法"""

        try:
            resp = self.client.request(
                method,
                f'http://{self.host}:{self.port}/{path}',
                headers= headers,
                params= params,
                data = payload,
                files= files,
                timeout= self.timeout
            )
            resp.raise_for_status()
        except requests.exceptions.HTTPError as e:
            if isinstance(e, requests.exceptions.Timeout):
                logger.warning(f'响应超时! => {e}')
            elif isinstance(e, requests.exceptions.HTTPError):
                logger.error(f'响应码出错 => {resp.status_code}')
            else:
                logger.error(f'请求出错')
            
            print(resp.text)
            return {}

        # 处理数据
        try:
            data = resp.json()
        except json.JSONDecodeError as e:
            logger.error('响应结果非json格式')
            print(resp.text)
            return {}
        
        if data is None:
            logger.error('返回为 NULL')
            return {}

        return data

    def post(
        self,
        path: str,
        params: dict = None,
        payload: dict = None,
        headers: dict = None,
        files: dict = None,
    ):
        return self.baseRequest(
            'POST',
            path,
            params= params, payload= payload, headers= headers, files= files
        )  

    def get(
        self,
        path: str,
        params: dict = None,
        payload: dict = None,
    ):
        return self.baseRequest(
            'GET',
            path,
            params= params, payload= payload
        )